import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jle-lib-a-component-a',
  templateUrl: './component-a.component.html',
  styleUrls: ['./component-a.component.css']
})
export class ComponentAComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
