import { NgModule } from '@angular/core';
import { JleAngularLibAComponent } from './jle-angular-lib-a.component';
import {ComponentAComponent} from '../../components/component-a/component-a.component';



@NgModule({
  declarations: [
    JleAngularLibAComponent,
    ComponentAComponent,
  ],
  imports: [
  ],
  exports: [
    JleAngularLibAComponent,
    ComponentAComponent,
  ]
})
export class JleAngularLibAModule { }
