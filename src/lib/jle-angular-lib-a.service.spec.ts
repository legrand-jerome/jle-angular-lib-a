import { TestBed } from '@angular/core/testing';

import { JleAngularLibAService } from './jle-angular-lib-a.service';

describe('JleAngularLibAService', () => {
  let service: JleAngularLibAService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JleAngularLibAService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
