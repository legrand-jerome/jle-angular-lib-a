import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JleAngularLibAComponent } from './jle-angular-lib-a.component';

describe('JleAngularLibAComponent', () => {
  let component: JleAngularLibAComponent;
  let fixture: ComponentFixture<JleAngularLibAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JleAngularLibAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JleAngularLibAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
