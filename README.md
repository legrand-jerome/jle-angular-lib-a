# JleAngularLibA

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Code scaffolding

Run `ng generate component component-name --project jle-angular-lib-a` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project jle-angular-lib-a`.
> Note: Don't forget to add `--project jle-angular-lib-a` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build jle-angular-lib-a` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build jle-angular-lib-a`, go to the dist folder `cd dist/jle-angular-lib-a` and run `npm publish`.

## Running unit tests

Run `ng test jle-angular-lib-a` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
